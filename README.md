## How to install

* copy .env.example to .env
* lando start
* lando composer install
* lando php artisan migrate
* lando php artisan admin:install
* lando php artisan cache:clear
* lando php artisan products:generate

## Requirements

You need to have lando locally installed to begin the project

https://lando.dev/

## Login

admin/admin

## Images of projects

See project images in /Screenshots folder

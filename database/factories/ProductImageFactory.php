<?php

namespace Database\Factories;

use App\Models\Product;
use App\Models\ProductImage;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * Class ProductImageFactory
 *
 * @package Database\Factories
 */
class ProductImageFactory extends Factory
{

    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ProductImage::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $faker = \Faker\Factory::create();
        $products = Product::all()->random(1)->pluck('id');

        return [
          'path' => $faker->imageUrl(),
          'product_id' => !empty($products) ? $products[0] : NULL,
        ];
    }

}

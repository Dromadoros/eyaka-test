<?php

namespace Database\Factories;

use App\Models\Product;
use App\Models\ProductCategory;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * Class ProductFactory
 *
 * @package Database\Factories
 */
class ProductFactory extends Factory
{

    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Product::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $faker = \Faker\Factory::create();
        $priceHtva = $faker->randomFloat(2, 1, 100);

        return [
          'name' => $faker->text(25),
          'reference' => $faker->text(25),
          'description' => $faker->text(),
          'quantity' => $faker->numberBetween(1, 500),
          'price_htva' => $priceHtva,
          'price' => $priceHtva + ($priceHtva * 0.21),
          'active' => 1,
        ];
    }

}

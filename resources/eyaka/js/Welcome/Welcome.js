import React, {Component} from 'react';
import {Container, Box, Button} from "@material-ui/core";

class Welcome extends React.Component {
    render() {
        return (
            <div className="wrapper">
                <div className={"overlay"} />
                <Container maxWidth="sm">
                    <Box component="span" m={1}>
                        <Button className={"cta"} variant="contained" color="primary" href={"/admin"}>
                            Se connecter
                        </Button>
                    </Box>
                </Container>
            </div>
        );
    }
}

export default Welcome

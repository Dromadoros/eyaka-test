import React, { Component } from 'react'
import ReactDOM from "react-dom";
import Welcome from "./Welcome";

const welcome = document.getElementById('welcome');

ReactDOM.render(
    <Welcome/>,
    welcome
);

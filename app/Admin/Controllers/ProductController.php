<?php

namespace App\Admin\Controllers;

use App\Models\Product;
use App\Models\ProductCategory;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

/**
 * Class ProductController
 *
 * @package App\Admin\Controllers
 */
class ProductController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Product';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Product());

        $grid->column('id', __('Id'));
        $grid->column('name', __('Name'));
        $grid->column('reference', __('Reference'));
        $grid->column('quantity', __('Quantity'));
        $grid->column('price', __('Price'));
        $grid->column('active', __('Active'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Product::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));
        $show->field('name', __('Name'));
        $show->field('reference', __('Reference'));
        $show->field('description', __('Description'));
        $show->field('quantity', __('Quantity'));
        $show->field('price_htva', __('Price htva'));
        $show->field('price', __('Price'));
        $show->field('active', __('Active'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Product());

        $form->column(8, function(Form $form) {
            $form->text('name', __('Name'));
            $form->text('reference', __('Reference'));
            $form->multipleFile('images','Attachments')->pathColumn('path')->removable();
            $form->textarea('description', __('Description'));
            $form->number('quantity', __('Quantity'));
            $form->decimal('price_htva', __('Price htva'));
            $form->decimal('price', __('Price'));
        });

        $form->column(4, function(Form $form) {
            $form->multipleSelect('categories')->options(ProductCategory::all()->pluck('name', 'id'));
            $form->switch('active', __('Active'));
        });

        return $form;
    }
}

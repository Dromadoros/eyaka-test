<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Product
 *
 * @package App\Models
 */
class Product extends Model
{

    use HasFactory;

    /**
     * @var array
     */
    protected $fillable = [
      'name',
      'description',
      'reference',
      'quantity',
      'price_htva',
      'price',
      'active',
    ];

    /**
     * @var array
     */
    protected $casts = [
      'active' => 'boolean',
      'price' => 'float',
      'priceHtva' => 'float',
      'quantity' => 'integer',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function images()
    {
        return $this->hasMany(ProductImage::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function categories()
    {
        return $this->belongsToMany(
          'App\Models\ProductCategory',
          'product_categories_relations',
          'product_id',
          'category_id'
        );
    }

}

<?php

namespace App\Models;

use Encore\Admin\Traits\AdminBuilder;
use Encore\Admin\Traits\ModelTree;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ProductCategory
 *
 * @package App\Models
 */
class ProductCategory extends Model
{

    use HasFactory;
    use ModelTree, AdminBuilder;

    /**
     * @var array
     */
    protected $fillable = [
      'name',
      'weight',
    ];

    protected $casts = [
      'weight' => 'integer',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function product()
    {
        return $this->belongsToMany(
          'App\Models\Product',
          'product_categories_relations',
          'category_id',
          'product_id'
        );
    }

}

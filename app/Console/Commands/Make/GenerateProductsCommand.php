<?php

namespace App\Console\Commands\Make;

use App\Models\Product;
use App\Models\ProductCategory;
use App\Models\ProductImage;
use Illuminate\Console\Command;

/**
 * Class GenerateProductsCommand
 *
 * @package App\Console\Commands\Make
 */
class GenerateProductsCommand extends Command
{

    const NUMBER_PRODUCTS = 5;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'products:generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate all products';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        Product::factory(10)->create();
        ProductCategory::factory(10)->create();
        ProductImage::factory(10)->create();

        $categories = ProductCategory::all();

        Product::all()->each(function (Product $product) use ($categories) {
            $product->categories()->attach(
              $categories->random(rand(1, 3))->pluck('id')->toArray()
            );
        });
    }

}
